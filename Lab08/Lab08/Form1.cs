using System.Windows.Forms;

namespace Lab08
{
    public partial class Form1 : Form
    {
        private Bitmap bmp;
        private Pen blackPen = new Pen(Color.Black, 4);
        private Graphics g;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public static List<Image> SplitImageByChannel(Image originalImage)
        {
            List<Image> imageFragments = new List<Image>();
            Bitmap bitmap = new Bitmap(originalImage);

            // Iterate through each color channel: R, G, B
            for (int channel = 0; channel < 3; channel++)
            {
                Bitmap channelBitmap = new Bitmap(bitmap.Width, bitmap.Height);

                // Iterate through each pixel in the original image
                for (int x = 0; x < bitmap.Width; x++)
                {
                    for (int y = 0; y < bitmap.Height; y++)
                    {
                        // Get the color of the pixel in the original image
                        Color color = bitmap.GetPixel(x, y);

                        // Create a new color with only the desired color channel value
                        Color channelColor = Color.FromArgb(
                            channel == 0 ? color.R : 0, // R channel
                            channel == 1 ? color.G : 0, // G channel
                            channel == 2 ? color.B : 0  // B channel
                        );

                        // Set the pixel color in the channel bitmap
                        channelBitmap.SetPixel(x, y, channelColor);
                    }
                }

                // Add the channel bitmap as a new image fragment to the list
                imageFragments.Add(channelBitmap);
            }

            return imageFragments;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image files (*.BMP, *.JPG, *.GIF, *.TIF, *.PNG, *.ICO, *.EMF, *.WMF)| *.bmp; *.jpg; *.gif; *.tif; *.png; *.ico; *.emf; *.wmf";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Image image = Image.FromFile(dialog.FileName);
                List<Image> splittedImages = SplitImageByChannel(image);
                Image currentImage;
                int width, height;
                Console.WriteLine(splittedImages.Count);
                for (int i = 0; i < splittedImages.Count; i++)
                {
                    currentImage = splittedImages[i];
                    width = currentImage.Width;
                    height = currentImage.Height;
                    switch(i)
                    {
                        case 0:
                            pictureBox1.Width = width;
                            pictureBox1.Height = height;
                            bmp = new Bitmap(image, width, height);
                            pictureBox1.Image = bmp;
                            break;
                        case 1:
                            pictureBox2.Width = width;
                            pictureBox2.Height = height;
                            bmp = new Bitmap(image, width, height);
                            pictureBox2.Image = bmp;
                            break;
                        case 2:
                            pictureBox3.Width = width;
                            pictureBox3.Height = height;
                            bmp = new Bitmap(image, width, height);
                            pictureBox3.Image = bmp;
                            break;
                    }
                }
                //g = Graphics.FromImage(pictureBox1.Image);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }
    }
}