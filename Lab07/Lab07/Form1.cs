﻿using System;
using System.Drawing;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;

namespace Lab07
{
    public partial class Form1 : Form
    {
        int x = 0;
        int y;
        Graphics gr;


        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            x = x + 2;
            y = 5 * x + 20;
            DrawCustomBazier();
            Refresh();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void DrawCustomBazier()
        {
            // Define four control points for the Bezier curve
            Point p1 = new Point(400 + x, 2 + y);
            Point p2 = new Point(450 + x, 10 + y);
            Point p3 = new Point(500 + x, 50 + y);
            Point p4 = new Point(350 + x, 100 + y);

            // Draw the Bezier curve using the Graphics object
            gr.DrawBezier(Pens.Black, p1, p2, p3, p4);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            gr = this.pictureBox1.CreateGraphics();
            this.timer1.Start();
        }
    }
}
