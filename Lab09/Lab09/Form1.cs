namespace Lab09
{
    public partial class Form1 : Form
    {
        private Graphics graphics;
        private Brush brush = new SolidBrush(Color.Black);
        private int x1, y1, width, height;
        private int xTemp, yTemp;

        public Form1()
        {
            InitializeComponent();
            graphics = panel1.CreateGraphics();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            drawRectangle();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            x1 = panel1.Width / 2;
            y1 = panel1.Height / 2;
            width = 50;
            height = 50;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (x1 == panel1.Width / 2 && y1 == panel1.Height / 2)
            {
                y1 = y1 + height;
            } else if (x1 == panel1.Width / 2 && y1 == panel1.Height / 2 + height)
            {
                x1 = x1 - width;
            } else if (x1 == panel1.Width / 2 - width && y1 == panel1.Height / 2 + height)
            {
                y1 = panel1.Height / 2;
            } else if (x1 == panel1.Width / 2 - width && y1 == panel1.Height / 2)
            {
                x1 = panel1.Width / 2;
            }
            drawRectangle();
            panel1.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            drawReset(false);
            width = width * 2;
            height = height * 2;
            drawRectangle();
            panel1.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            drawReset(true);
        }

        private void drawRectangle()
        {
            Rectangle rectangle = new Rectangle(x1, y1, width, height);
            graphics.FillRectangle(brush, rectangle);
        }

        private void drawReset(Boolean sizeReset)
        {
            x1 = panel1.Width / 2;
            y1 = panel1.Height / 2;
            if (sizeReset)
            {
                width = 50;
                height = 50;
            }
            drawRectangle();
            panel1.Refresh();
        }
    }
}