﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab06
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Pen pen = new Pen(Color.White, 2);
            SolidBrush redBrush = new SolidBrush(Color.Red);

            for (int i = 0; i < 50; i++)
            {
                g.DrawLine(new Pen(Brushes.Black, 2), 10, 4 * i + 20, 200, 4 * i + 20);
            }

            g.DrawEllipse(pen, 100, 150, 100, 100);
            g.FillRectangle(redBrush, 140, 35, 20, 40);
            pen.Color = Color.Green;
            pen.Width = 10;
            pen.DashStyle = DashStyle.DashDotDot;
            g.DrawLine(pen, new Point(110), new Point(1000000));
            
        }
    }
}
